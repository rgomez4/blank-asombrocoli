import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import {TicketService} from '../ticket.service'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  email:string
  clave:string
  perfil:string

  constructor(public nav:NavController, public servicio:TicketService) {}

  onPerfil(evento){
    console.log("perfil:" ,evento.detail.value);

    if(evento.detail.value=="profesor"){
      this.email="profesor@profesor.com"
      this.clave="1234"
      this.perfil="profesor"
    }
    else{
      this.email ="tecnico@tecnico.com"
      this.clave ="perfiltecnico"
      this.perfil ="tecnico"

    }
  }

  ingresar(){
    this.servicio.validar_ingreso(this.email, this.clave).subscribe(
      respuesta => {
        if (respuesta["mensaje"]!= null){
          if (respuesta["perfil"]=="profesor") {
            this.nav.navigateForward("formulario-ticket", {
              queryParams:{
              nombre: respuesta["nombre"],
              apellido: respuesta["apellido"],
              email: respuesta["email"]
              }
            })
          } else {
            this.nav.navigateForward("listatikcets", {
              queryParams:{
              nombre: respuesta["nombre"],
              apellido: respuesta["apellido"],
              email: respuesta["email"]
              }
            })
          }
          
      }
    });
  }











}
