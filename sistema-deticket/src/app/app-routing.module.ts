import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'formulario-ticket',
    loadChildren: () => import('./Profesor/formulario-ticket/formulario-ticket.module').then( m => m.FormularioTicketPageModule)
  },
  {
    path: 'listatickets',
    loadChildren: () => import('./Técnico/listatickets/listatickets.module').then( m => m.ListaticketsPageModule)
  },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
