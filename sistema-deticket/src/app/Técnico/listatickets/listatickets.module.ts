import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaticketsPageRoutingModule } from './listatickets-routing.module';

import { ListaticketsPage } from './listatickets.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListaticketsPageRoutingModule
  ],
  declarations: [ListaticketsPage]
})
export class ListaticketsPageModule {}
