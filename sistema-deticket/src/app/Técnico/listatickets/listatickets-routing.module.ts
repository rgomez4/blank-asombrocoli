import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaticketsPage } from './listatickets.page';

const routes: Routes = [
  {
    path: '',
    component: ListaticketsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaticketsPageRoutingModule {}
