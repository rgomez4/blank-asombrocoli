import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  url ="";

  constructor(private http:HttpClient) { }


  validar_ingreso(email, clave){
    let datos = {
      email: email,
      clave: clave
    }

    let header = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Headers":'Origin, Content-Type',
      "Access-Control-Allow-Origin": "*",
      'Access-Control-Allow-Methods': 'POST',

    });
    let options = {headers:header}
    return this.http.post(this.url + "login", datos, options);
  }





}
